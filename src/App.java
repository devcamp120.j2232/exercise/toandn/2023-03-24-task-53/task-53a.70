import module.Time;

public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(23, 24, 30);

        Time time2 = new Time(20, 24, 30);

        System.out.println("Time 1: " + time1.toString());
        System.out.println("Tang Time 1 len 1s: " + time1.nextSecond());
        System.out.println("Giam Time 1 xuong 1s: " + time1.previousSecond());

        System.out.println("Time 2: " + time2.toString());
        System.out.println("Tang Time 2 len 1s: " + time2.nextSecond());
        System.out.println("Giam Time 2 xuong 1s: " + time2.previousSecond());
    }
}
